function flight(airline, number, origin, destination, dep_time, arrival_time, arrival_gate) {
  this.airline = airline;
  this.number = number;
  this.origin = origin;
  this.destination = destination;
  this.dep_time = dep_time;
  this.arrival_time = arrival_time;
  this.arrival_gate = arrival_gate;
  this.flight_duration = function () {
    return this.arrival_time - this.dep_time;
  }
}

var alaska = new flight('Alaska (AK333)', 'B737', 'KSEA', 'KGEG', 'NOV 11, 2016 18:00:00', 'NOV 11, 2016 19:20:00', 'C7');
var united = new flight('United (U2001)', 'A320', 'KORD', 'NYC', 'NOV 11, 2016 01:10:00', 'NOV 11, 2016 05:25:00', 'N17');
var delta = new flight('Delta (DAL42)', 'A320', 'KPDX', 'KLAX', 'NOV 11, 2016 09:15:00', 'NOV 11, 2016 11:40:00', 'S5');
var flights = [alaska, united, delta];

function millisToMinutesAndSeconds(millis) {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}

var a_date1 = new Date(flights[0]['dep_time']);
var a_date2 = new Date(flights[0]['arrival_time']);
var a_diff = a_date2.getTime() - a_date1.getTime();

var u_date1 = new Date(flights[1]['dep_time']);
var u_date2 = new Date(flights[1]['arrival_time']);
var u_diff = u_date2.getTime() - u_date1.getTime();

var d_date1 = new Date(flights[2]['dep_time']);
var d_date2 = new Date(flights[2]['arrival_time']);
var d_diff = d_date2.getTime() - d_date1.getTime();

var el = document.getElementById('table_data');
el.innerHTML = "<th>Airline</th> <th>Number</th> <th>Origin</th> <th>Destination</th> <th>Departure Time</th> <th>Arrival Time</th> <th>Arrival Gate</th> <th>Flight Duration (minutes)</th>";
var i = 0;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] + "</td><td>" + flights[i]['origin'] + "</td><td>" + flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] + "</td><td>" + flights[i]['arrival_gate'] + "</td><td>" + millisToMinutesAndSeconds(a_diff); + "</td></tr>";

i = 1;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] + "</td><td>" + flights[i]['origin'] + "</td><td>" + flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] + "</td><td>" + flights[i]['arrival_gate'] + "</td><td>" + millisToMinutesAndSeconds(u_diff); + "</td></tr>";

i = 2;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] + "</td><td>" + flights[i]['origin'] + "</td><td>" + flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] + "</td><td>" + flights[i]['arrival_gate'] + "</td><td>" + millisToMinutesAndSeconds(d_diff); + "</td></tr>";