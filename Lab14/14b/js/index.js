var stocks = new Array;

stocks.push({
  cName: "Microsoft",
  mCap: "$381.7 B",
  sales: "$86.8 B",
  profit: "$22.1 B",
  employeesData: "128,000"
});

stocks.push({
  cName: "Symetra Finnacial",
  mCap: "$2.7 B",
  sales: "$2.2 B",
  profit: "$254.4 M",
  employeesData: "1,400"
});

stocks.push({
  cName: "Micron Technology",
  mCap: "$37.6 B",
  sales: "$16.4 B",
  profit: "$3.0 B",
  employeesData: "30,400"
});

stocks.push({
  cName: "F5 Networks",
  mCap: "$9.5 B",
  sales: "$1.7 B",
  profit: "$311.2 M",
  employeesData: "3,834"
});

stocks.push({
  cName: "Expedia",
  mCap: "$10.8 B",
  sales: "$5.8 B",
  profit: "$398.1 M",
  employeesData: "18,210"
});

stocks.push({
  cName: "Nautilus",
  mCap: "$476 M",
  sales: "$274.4 M",
  profit: "$18.8 M",
  employeesData: "340"
});

stocks.push({
  cName: "Heritage Financial",
  mCap: "$531 M",
  sales: "$137.6 M",
  profit: "$21 M",
  employeesData: "748"
});

stocks.push({
  cName: "Cascade Microtech",
  mCap: "$239 M",
  sales: "$136 M",
  profit: "$9.9 M",
  employeesData: "449"
});

stocks.push({
  cName: "Nike",
  mCap: "$83.1 B",
  sales: "$27.8 B",
  profit: "$2.7 B",
  employeesData: "56,500"
});

stocks.push({
  cName: "Alaska Air Group",
  mCap: "$7.9 B",
  sales: "$5.4 B",
  profit: "$605 M",
  employeesData: "13,952"
});

var el = document.getElementById("data");

function myFunction(name, index) {

el.innerHTML += "<tr><td>" +
  name.cName + "</td><td>" +
  name.mCap + "</td><td>" +
  name.sales + "</td><td>" +
  name.profit + "</td><td>" +
  name.employeesData + "</td>" + "<tr />";
}