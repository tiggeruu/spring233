function calcCircleGeometries(radius) {
  const pi = Math.PI;
  var area = pi * radius * radius;
  var circumference = 2 * pi * radius;
  var diameter = 2 * radius;
  var geometries = [area, circumference, diameter];
  return geometries;
}

var rad1 = Math.random();
var rad2 = Math.random();
var rad3 = Math.random();

var g1 = calcCircleGeometries(rad1);
var g2 = calcCircleGeometries(rad2);
var g3 = calcCircleGeometries(rad3);

console.log(rad1, g1);
console.log(rad2, g2);
console.log(rad3, g3);

var bleh1 = "<td>" + rad1 + "</td>" + "<td>" + g1[0] + "</td>" + "<td>" + g1[1] + "</td>" + "<td>" + g1[2] + "</td>";
var bleh2 = "<td>" + rad2 + "</td>" + "<td>" + g2[0] + "</td>" + "<td>" + g2[1] + "</td>" + "<td>" + g2[2] + "</td>";
var bleh3 = "<td>" + rad3 + "</td>" + "<td>" + g3[0] + "</td>" + "<td>" + g3[1] + "</td>" + "<td>" + g3[2] + "</td>";

var el = document.getElementById("n1");
el.innerHTML = bleh1;
var el = document.getElementById("n2");
el.innerHTML = bleh2;
var el = document.getElementById("n3");
el.innerHTML = bleh3;